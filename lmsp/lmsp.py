#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import re
from typing import List

import click
from slugify import slugify

GRAPHIC_CONFIG = "width=\\textwidth,height=\\textheight,keepaspectratio"


class Latex():
    content: str = ""
    scope_list: list = []

    def tabulation(self):
        self.content += " " * (len(self.scope_list) * 4)

    def empty_scope(self):
        self.raw('{')
        self.scope_list.append("")

    def scope(self, scope: str, array: str = "", args: list = []):
        self.command("begin", scope, array, args)
        self.scope_list.append(scope)

    def end_scope(self):
        scope = self.scope_list.pop()
        if len(scope):
            self.command("end", scope)
        else:
            self.raw('}')

    def command(self, command: str, main_arg: str = "", array: str = "", args: list = [], post_args: str = ""):
        arg_res = ""
        for arg in args:
            arg_res += '{' + arg + '}'

        self.tabulation()
        self.content += '\\' + command + ("{" + main_arg + '}' if main_arg else "") + arg_res + \
                        ("[" + array + ']' if array else "") + post_args + "\n"

    def complex_command(self, command: str, content: str):
        self.tabulation()
        self.content += f"\\{command}{content}\n"

    def raw(self, text: str):
        self.tabulation()
        self.content += text + '\n'

    def include(self, latex: 'Latex'):
        self.content += latex.content + '\n'

    def skip_line(self):
        self.content += '\n'


class Slide:
    title: str = ""
    picture: str = ""
    text: str = ""
    id: str = ""

    def __init__(self, title: str, picture: str, text: str, id: str):
        self.title = title if title else ""
        self.picture = picture if picture else ""
        self.text = text if text else ""
        self.id = id if id else "UNKNOWN ID"

    def to_latex(self, debug: bool = False) -> Latex:
        slide: Latex = Latex()

        if debug:
            slide.empty_scope()
            slide.command("setbeamertemplate", "footline", args=[self.id])

        slide.scope("frame", args=[self.title])

        if self.picture:
            slide.scope("figure", array="p")
            # Assume text will fit on 3 lines
            slide.command("includegraphics", array="width=\\textwidth,height=\\dimexpr \\textheight - " + (
                "6" if self.title else "4") + "\\baselineskip\\relax,keepaspectratio",
                          post_args=f"{{{self.picture}}}")

            if self.text:
                slide.command("caption", main_arg=self.text)

            slide.end_scope()
        else:
            slide.raw(self.text)

        slide.end_scope()

        if debug:
            slide.end_scope()

        return slide


class Section:
    title: str = ""
    directory: str = ""
    path: str = ""
    full_path: str = ""
    slides: List[Slide] = []
    subsections: List['Section'] = []
    is_subsection = False

    def __init__(self, title, directory, path='', chapter='', is_subsection: bool = False):
        self.title = title
        self.directory = directory
        self.path = path
        self.full_path = f"{path}data/{directory}" if not chapter else f"{path}data/{chapter}/{directory}"
        self.slides = []
        self.subsections = []
        self.__get_content()
        self.is_subsection = is_subsection

        if not is_subsection:
            self.__get_subsections()

    def __get_content(self):
        pictures: list = [f"{self.full_path}/{f}" for f in os.listdir(self.full_path) if
                          re.match(r'.*\.((png)|(jpg))', f)]
        texts: list = [f"{self.full_path}/{f}" for f in os.listdir(self.full_path) if re.match(r'.*\.txt', f)]
        titles: list = [f"{self.full_path}/{f}" for f in os.listdir(self.full_path) if re.match(r'.*\.title', f)]

        total: set = {os.path.splitext(x)[0]
                      for x in titles + texts + pictures}

        files: list = list(total)
        files.sort()

        for f in files:
            name, _ = os.path.splitext(os.path.basename(f))

            id = f"{self.full_path}/{name}"

            title = ""
            text = ""

            # Get picture path
            pic_png_path = f"{id}.png"
            pic_jpg_path = f"{id}.jpg"

            picture = pic_png_path if pic_png_path in pictures else pic_jpg_path if pic_jpg_path in pictures else None
            # Get text
            text_path = f"{id}.txt"
            if text_path in texts:
                with open(f"{text_path}", 'r') as file:
                    text = escape_char(file.read())

            # Get title
            title_path = f"{id}.title"
            if title_path in titles:
                with open(f"{title_path}", 'r') as file:
                    title = escape_char(file.read())

            self.slides.append(
                Slide(title, picture, text, slugify(id)))

    def __get_subsections(self):
        pass

    def to_latex(self, debug: bool = False) -> Latex:
        section: Latex = Latex()

        for slide in self.slides:
            section.include(slide.to_latex(debug))

        if not self.is_subsection:
            for subsection in self.subsections:
                section.command("subsection", subsection
                                .title)
                section.include(subsection.to_latex(debug))

        return section


# ============= #
#               #
#   M A I N     #
#               #
# ============= #


def escape_char(text: str):
    return text.replace("\\", "\\textbackslash ") \
        .replace("$", "\\$") \
        .replace("_", "\\_") \
        .replace("&", "\\&") \
        .replace("~", "$\\sim$") \
        .replace("²", "\\") \
        .strip()


def parse_config(path: str, config_file: str) -> dict:
    with open(f"{path}{config_file}") as json_file:
        json_data = json.load(json_file)

    config: dict = {}

    # Config options
    config_list = ["title", "author", "displayChapter",
                   "usecolortheme", "usetheme"]
    for c in config_list:
        config[c] = json_data[c]

    # Chapters
    config["chapters"]: dict = {}

    for chap_name, chapter in json_data["chapters"].items():
        config["chapters"][chap_name]: List[Section] = []

        for k, v in chapter["sections"].items():
            section: Section = Section(title=v, directory=k, chapter=chap_name, path=path)

            if "subsections" in chapter:
                if k in chapter["subsections"].keys():
                    for sub_k, sub_v in chapter["subsections"][k].items():
                        section.subsections.append(
                            Section(title=sub_v, chapter=chap_name, directory=f"{k}/{sub_k}", path=path,
                                    is_subsection=True))

            config["chapters"][chap_name].append(section)

    return config


def gen_main_page(config: dict) -> Latex:
    main: Latex = Latex()

    main.complex_command("documentclass", "[aspectratio=169]{beamer}")
    main.command("usetheme", config["usetheme"])
    main.command("usecolortheme", config["usecolortheme"])
    main.complex_command("usepackage", "[T1]{fontenc}")
    main.command("title", config["title"])
    main.command("date", "\\today")
    main.command("author", config["author"])

    main.complex_command("AtBeginSection", "{\\frame{\\sectionpage}}")
    main.complex_command("AtBeginSubsection", "{\\frame{\\subsectionpage}}")
    main.complex_command("AtBeginSubsubsection",
                         "{\\frame{\\subsubsectionpage}}")

    main.command("setbeamertemplate", "caption", args=["\\insertcaption"])

    main.complex_command(
        "newcommand", "{\\atoc}[1]{\\addtocontents{toc}{#1\\par}}")
    main.complex_command("renewcommand", "{\\thesection}{\\arabic{section}.}")

    main.scope("document")

    main.command("maketitle")

    main.scope("frame", "allowframebreaks")
    main.command("tableofcontents")
    main.end_scope()  # frame

    main.command("makeatletter")
    main.skip_line()

    for chapter in config["chapters"]:
        if config["show"] and config["show"] != chapter:
            continue

        if config["displayChapter"]:
            main.command("atoc", "\\alert{" + chapter + "}")

        for section in config["chapters"][chapter]:
            main.command("section", section.title)
            main.command("input", slugify(section.title))

    main.end_scope()  # document

    return main


def write_file(path: str, content: str) -> None:
    with open(f"{path}.tex", 'w') as f:
        f.write(content)


def lmsp(project_directory, debug: bool = False, only_chapter: str = None):
    # Getting the files
    root_dir: str = project_directory + \
                    ("" if project_directory == '/' else "/")
    config: dict = parse_config(root_dir, "config.json")
    config["show"] = only_chapter

    # Building the latex
    main_file: Latex = gen_main_page(config)
    write_file(f"{root_dir}{config['title']}", main_file.content)

    for chapter in config["chapters"]:
        if only_chapter and only_chapter != chapter:
            continue

        for section in config["chapters"][chapter]:
            section_file: Latex = section.to_latex(debug)
            write_file(f"{root_dir}{slugify(section.title)}", section_file.content)


@click.command(name="cli")
@click.option('--debug', is_flag=True, default=False)
@click.argument('project_directory', type=str)
def cli(project_directory, debug):
    return lmsp(project_directory, debug)

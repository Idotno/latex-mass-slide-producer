#!/usr/bin/env pytho

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="lmsp",  # Replace with your own username
    version="0.1.0",
    author="Valérian Fayt",
    author_email="valerian.fayt@epita.fr",
    description="Simple package to generate latex slides from pictures",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.7",
    install_requires=[
        'click==7.1.2',
        'python-slugify==4.0.1'
    ]
)
